<?php include('server.php'); ?>

<!DOCTYPE html>
<html lang="sh">
    
<head>
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8">
    <link rel="stylesheet" type="text/css" href="css/style.css">
    <link rel="icon" href="images/favicon.jpg">
    <title>Obrazac za registrovanje</title>
</head>

<body>
    <div class="container">
        <div class="header">
            <h1>Twilight<span class="off">Blue</span></h1>
            <h2>MALI OGLASI</h2>
        </div>

        <div class="menu">
            <ul>
                <li class="menuitem"><a href="#">Početna</a></li>
                <li class="menuitem"><a href="#">O nama</a></li>
                <li class="menuitem"><a href="#">Oglasi</a></li>
                <li class="menuitem"><a href="#">Usluge</a></li>
                <li class="menuitem"><a href="#">Zaposleni</a></li>
              <li class="menuitem"><a href="#">Kontakt</a></li>
            </ul>
        </div>

        <div class="leftmenu">
            <div class="leftmenu_top"></div>
            <div class="leftmenu_main">                
                <h3>Links</h3>
                <ul>
                    <li><a href="index.php">Početna</a></li>
                    
                    <?php
                    if (empty($_SESSION['username'])) {
                        echo '<li><a href="login.php">Uloguj se</a></li>';
                        echo '<li><a href="register.php">Registruj nalog</a></li>';
                    }                        
                    else {
                        echo '<li><a href="newad.php">Dodaj novi oglas</a></li><br>';
                        echo '<p class="welcome">Dobrodošli, <strong>';
                        echo $_SESSION['username'];
                        echo '</strong></p>';
                        echo '<li><a href="index.php?logout=\'1\'" style="color:maroon";>Izloguj se</a></li>';
                    }
                    ?>                 
                    
                </ul>
            </div>            
            <div class="leftmenu_bottom"></div>
        </div>
        <div class="content">
            <div class="content_top"></div>
            <div class="content_main" style="height:500px">
                <h2 class="title-login">Registrujte se</h2>                
                <form method="post" action="register.php">
                    <!-- display validation errors here -->
                    <?php include('errors.php'); ?>
                    <div class="input-group">
                        <label>Korisničko ime</label><br>
                        <input type="text" name="username">
                    </div>
                    <div class="input-group">
                        <label>Email</label><br>
                        <input type="email" name="email">
                    </div>
                    <div class="input-group">
                        <label>Lozinka</label><br>
                        <input type="password" name="password_1">
                    </div>
                    <div class="input-group">
                        <label>Potvrdi lozinku</label><br>
                        <input type="password" name="password_2">
                    </div>
                    <div class="input-group">            
                        <button type="submit" name="register" class="btn">Registruj se</button>
                    </div>
                    <p>
                        Imate nalog?<br><a href="login.php"><span class="button">Uloguj se</span></a>
                    </p>
                </form>                
            </div>
            <div class="content_bottom"></div>
            <div class="footer"><h3><a href="#">MySQL and PHP</a></h3></div>
        </div>
    </div>    
</body>
    
</html>
