<?php include('server.php');
$subject = $price = $description = $category = $currency = $phone = "";

// do not allow unregistered user to access this page
// send her/him to index page to sign-up/in or just browse through ads
if (!isset($_SESSION["username"])) {
    header('location: index.php');
}
?>

<!DOCTYPE html>
<html lang="sh">
    
<head>
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8">
    <link rel="stylesheet" type="text/css" href="css/style.css">
    <link rel="icon" href="images/favicon.jpg">
    <title>Dodaj novi oglas</title>
</head>

<body>
    <div class="container">
        <div class="header">
            <h1>Twilight<span class="off">Blue</span></h1>
            <h2>MALI OGLASI</h2>
        </div>

        <div class="menu">
            <ul>
                <li class="menuitem"><a href="#">Početna</a></li>
                <li class="menuitem"><a href="#">O nama</a></li>
                <li class="menuitem"><a href="#">Oglasi</a></li>
                <li class="menuitem"><a href="#">Usluge</a></li>
                <li class="menuitem"><a href="#">Zaposleni</a></li>
              <li class="menuitem"><a href="#">Kontakt</a></li>
            </ul>
        </div>

        <div class="leftmenu">
            <div class="leftmenu_top"></div>
            <div class="leftmenu_main">                
                <h3>Links</h3>
                <ul>
                    <li><a href="index.php">Početna</a></li>
                    
                    <?php
                    if (empty($_SESSION['username'])) {
                        echo '<li><a href="login.php">Uloguj se</a></li>';
                        echo '<li><a href="register.php">Registruj nalog</a></li>';
                    }                        
                    else {
                        echo '<li><a href="newad.php">Dodaj novi oglas</a></li><br>';
                        echo '<p class="welcome">Dobrodošli, <strong>';
                        echo $_SESSION['username'];
                        echo '</strong></p>';
                        echo '<li><a href="index.php?logout=\'1\'" style="color:maroon";>Izloguj se</a></li>';
                    }
                    ?>                 
                    
                </ul>
            </div>            
            <div class="leftmenu_bottom"></div>
        </div>
        <div class="content">
            <div class="content_top"></div>
            <div class="content_main">                
                <form class="enter" action="server.php" method="POST" name="upload" enctype="multipart/form-data">
                    <fieldset class="fieldset">
                        <legend class="legend">Dodajte novi oglas:</legend>
                        <!-- display validation errors here -->
                        <?php include('errors.php'); ?>
                        <label>Naslov oglasa</label><br>
                        <input type="text" name="subject" class="std-input" required><br><br>
                        <label>Broj telefona</label><br>
                        <input type="text" name="phone" class="std-input" required><br><br>
                        <label>Opis oglasa</label><br>
                        <textarea name="description" class="std-input" rows="12" cols="37" required></textarea><br><br>
                        <label>Cena</label>
                        <?php
                        $sql = "SELECT currency FROM currency";
                        $result = mysqli_query($db, $sql);                    
                        echo "<select name='currency' required>";
                        while ($row = mysqli_fetch_assoc($result)) {
                            echo "<option value='" . $row['currency'] . "'>" . $row['currency'] . "</option>";
                        }
                        echo "</select><br><br>";                
                        ?>     
                        <input type="text" name="price" class="std-input" required><br><br> 
                        <label>Izaberi grupu kojoj oglas pripada</label><br><br>
                        <?php
                        $sql = "SELECT category FROM category";
                        $result = mysqli_query($db, $sql);                    
                        echo "<select name='category' required>";
                        while ($row = mysqli_fetch_assoc($result)) {
                            echo "<option value='" . $row['category'] . "'>" . $row['category'] . "</option>";
                        }
                        echo "</select><br><br>";                
                        ?>                    
                        <label for="file">Slika<span class="required"></span></label><br>
                        <input type="file" name="file" id="file" required><br><br>
                        <input type="submit" name="newad" id="submitbutton" value="Dodaj oglas">
                        <?php
                        echo "<a href=\"index.php\">Otkaži unos</a><br>";
                        ?>
                    </fieldset>
                </form>
                
            </div>
            <div class="content_bottom"></div>
            <div class="footer"><h3><a href="#">MySQL and PHP</a></h3></div>
        </div>
    </div>    
</body>
    
</html>
