-- phpMyAdmin SQL Dump
-- version 4.7.4
-- https://www.phpmyadmin.net/
--
-- Host: 127.0.0.1:3306
-- Generation Time: Apr 13, 2018 at 09:27 PM
-- Server version: 5.7.19
-- PHP Version: 5.6.31

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `advertisedb`
--

-- --------------------------------------------------------

--
-- Table structure for table `advertise`
--

DROP TABLE IF EXISTS `advertise`;
CREATE TABLE IF NOT EXISTS `advertise` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `subject` varchar(30) NOT NULL,
  `description` text NOT NULL,
  `price` decimal(15,2) NOT NULL,
  `currency_id` int(2) NOT NULL,
  `phone` varchar(20) NOT NULL,
  `picture` varchar(100) NOT NULL,
  `date` varchar(12) NOT NULL,
  `category_id` int(11) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=9 DEFAULT CHARSET=utf8;

--
-- Dumping data for table `advertise`
--

INSERT INTO `advertise` (`id`, `subject`, `description`, `price`, `currency_id`, `phone`, `picture`, `date`, `category_id`) VALUES
(1, 'ASUS X541UA-GO1113', 'ASUS X541UA-GO1113 (INTEL CORE I3-6006U, 8GB, 256GB SSD) OPIS Ekran 15.6 LED Back-lit, Ultra Slim 200nits, HD 1366x768, Glare Procesor Intel Core i3-6006U Processor, 2.0GHz (3M Cache) Memorija 8GB DDR4 2133MHz , maksimum 16GB Hard disk SATA 256G 2.5 SSD Graficka karta Intel HD graphics 520 Operativni sistem Endless Opticki uredjaj Nema opticki uredjaj Mreza LAN Fast Ethernet 10/100 Mbps Wireless LAN 802.11 b/g/n Bluetooth 4.0 Slotovi 1x USB 2.0 1x USB 3.0 1x USB3.1 Type C (gen 1)', '60000.00', 1, '065442233', 'files/images/proba.jpg', '1523648753', 5),
(2, 'Akcija Belinea tft 19 inca', 'Akcija Belinea tft 19 inca odlicna slika sa kablovima model - bj10001 VGA Dvi Audio in Dobijaju se naponski i vga kabal Nema mrtvih pixela, nema flekica Pogledajte sve moje oglase Ispravan i testiran saljem ili licno preuzimanje Cena transporta je oko 270 dinara pakuje se u kartonske kutije a ekran se zasticuje sa stiropolom Broj - 611', '220.00', 2, '024555666', 'files/images/1523649900-gordon.jpg', '1523649900', 5),
(3, 'Opel agila 1.2 na benzin', 'Opel agila 1.2, benzin, 2001. godiste, svetlo plava metalik,5 vrata. Automobil u odlicnom stanju, ocuvan. Sve od opreme. Sve 4 dobre gume. 2kljuca. Uvezen iz Nemacke. Ocarinjen i ima uverenje iz AMSS. Ide na ime kupca. Kupcu ostaje samo registracija.', '2000.00', 3, '038555666', 'files/images/1523650127-gordon.jpg', '1523650127', 4),
(4, '2.5 soban prozivka ', 'Lep 2,5 soban stan sa dve spavaÄ‡e sobe, i dnevni boravak sa kuhinjom, na treÄ‡em spratu od 8,u Pazinskoj ulici preko puta vrtica.', '40000.00', 4, '0665468981', 'files/images/1523650458-tosa.jpg', '1523650458', 3),
(5, 'Logo dizajn', 'Dizajniranje logotipa, brosura, kataloga, vizit kartica, flajera... Dizajn, po jako povoljim cenama, sa mnogo kreativnosti i maste koja ce ostati u secanju svakog posmatraca (potencijalnog kupca).', '15000.00', 1, '0644455667', 'files/images/1523651183-tosa.jpg', '1523651183', 2),
(6, 'VENTILATOR TITAN', 'BLIZI SE LETO ODLICAN ventilator sa TRI brzine i zakretanje krilaca automatski, necujan kao ORKAN duva. Ima i programator, postolje, dimenzije 38x42cm SJAJAN\r\n', '20.00', 2, '0115898956', 'files/images/1523651534-tosa.jpg', '1523651534', 5),
(7, 'ULTRAZVUCNI OSVEZIVAC PROSTORA', 'ULTRAZVUCNI  OVLAZIVAC  za bolje disanje i zdraviji san. Pogodan za upotrebu u decijim i bebi sobama do 15m2. Besuman i tih u radu.\r\n', '10.00', 2, '0112562354', 'files/images/1523660958-marko.jpg', '1523660958', 5),
(8, 'Tokio stan od 75 m2', 'Stan od 75 m2 odlicno uredjen na prvom spratu. Blizu obdaniste, skola. Lep, velik, prostran.\r\n', '34200.00', 2, '013156965', 'files/images/1523661223-tosa.jpg', '1523661223', 3);

-- --------------------------------------------------------

--
-- Table structure for table `category`
--

DROP TABLE IF EXISTS `category`;
CREATE TABLE IF NOT EXISTS `category` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `category` varchar(30) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=6 DEFAULT CHARSET=utf8;

--
-- Dumping data for table `category`
--

INSERT INTO `category` (`id`, `category`) VALUES
(1, 'Sport i hobi'),
(2, 'Usluge'),
(3, 'Nekretnine'),
(4, 'Vozila'),
(5, 'Elektronika');

-- --------------------------------------------------------

--
-- Table structure for table `currency`
--

DROP TABLE IF EXISTS `currency`;
CREATE TABLE IF NOT EXISTS `currency` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `currency` varchar(3) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=5 DEFAULT CHARSET=utf8;

--
-- Dumping data for table `currency`
--

INSERT INTO `currency` (`id`, `currency`) VALUES
(1, 'RSD'),
(2, 'EUR'),
(3, 'USD'),
(4, 'AUD');

-- --------------------------------------------------------

--
-- Table structure for table `users`
--

DROP TABLE IF EXISTS `users`;
CREATE TABLE IF NOT EXISTS `users` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `username` varchar(100) NOT NULL,
  `email` varchar(100) NOT NULL,
  `password` varchar(100) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=utf8;

--
-- Dumping data for table `users`
--

INSERT INTO `users` (`id`, `username`, `email`, `password`) VALUES
(1, 'Marko', 'marko@gmail.com', 'f68d1c0f60e16c06f454c39cf1377609'),
(2, 'Tosa', 'tosa@gmail.com', 'c46ff5465deb8372ba464697f1ec8b66');
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
