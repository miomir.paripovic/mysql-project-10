<?php
session_start();
$username = $email = $name = "";
$errors = array();
$salt1 = '$Bh^&+Z~3vM?.e$y';
$salt2 = 'Y-(dg[6fd<r.:8%p';


// connect to the database
$db = mysqli_connect('localhost', 'korisnik', 'korisnik123', 'advertisedb');

// if the register button is clicked
if (isset($_POST['register'])) {
    $username = mysqli_real_escape_string($db, $_POST['username']);
    $email = mysqli_real_escape_string($db, $_POST['email']);
    $password_1 = mysqli_real_escape_string($db, $_POST['password_1']);
    $password_2 = mysqli_real_escape_string($db, $_POST['password_2']);    
    
    // ensure that form fields are filled properly
    if (empty($username)) {
        array_push($errors, "Korisničko ime je obavezno");
    }
    if (empty($email)) {
        array_push($errors, "Email je obavezan");
    }
    if (empty($password_1)) {
        array_push($errors, "Lozinka je obavezna");
    }
    
    if ($password_1 != $password_2) {
        array_push($errors, "Unete lozinke se ne podudaraju");
    }
    
    // check if username and/or email are already in db
    $query = "SELECT * FROM users WHERE username='$username'";
    $result = mysqli_query($db, $query);
    if (mysqli_num_rows($result) > 0) {
        array_push($errors, "Korisničko ime je zauzeto!");
    }
    
    $query = "SELECT * FROM users WHERE email='$email'";
    $result = mysqli_query($db, $query);
    if (mysqli_num_rows($result) > 0) {
        array_push($errors, "Uneti email je zauzet!");
    }
    
    // if there are no errors, save user to database
    if (count($errors) == 0) {
        $password = md5($salt1.$password_1.$salt2); // encrypt password before storing in database (security)
        $sql = "INSERT INTO users (username, email, password)
                VALUES ('$username', '$email', '$password')";
        mysqli_query($db, $sql);
        $_SESSION['username'] = $username;
        $_SESSION['succes'] = "You are now logged in";
        header('location: index.php'); //redirect to home page
        
    }
}

// log user in from login page
if (isset($_POST['login'])) {
    $username = mysqli_real_escape_string($db, $_POST['username']);    
    $password = mysqli_real_escape_string($db, $_POST['password']);    
    
    // ensure that form fields are filled properly
    if (empty($username)) {
        array_push($errors, "Korisničko ime je obavezno");
    }
    if (empty($password)) {
        array_push($errors, "Lozinka je obavezna");
    }
    
    if (count($errors) == 0) {
        $password = md5($salt1.$password.$salt2); // encrypt password before comparing with that from database
        $query = "SELECT * FROM users WHERE username='$username' AND password='$password'";
        $result = mysqli_query($db, $query);
        if (mysqli_num_rows($result) == 1) {
            $_SESSION['username'] = $username;
            $_SESSION['succes'] = "Uspešno ste ulogovani";
            header('location: index.php'); //redirect to home page
        }
        else {
            array_push($errors, "Pogrešno korisničko ime/lozinka");
            //header('location: login.php');
        }
    }
}

// add a new ad :)    
if (isset($_POST['newad'])) {
    //var_dump($_FILES["file"]["tmp_name"]);        
    if (exif_imagetype($_FILES["file"]["tmp_name"])) {
        $file_name  = $_FILES["file"]["name"];
        $file_temp  = $_FILES["file"]["tmp_name"];
        $file_size  = $_FILES["file"]["size"];
        $file_type  = $_FILES["file"]["type"];
        $file_error = $_FILES["file"]["error"];

        $ext_temp  = explode(".", $file_name);
        $extension = end($ext_temp);

        $directory = "files/images/";
        //echo $directory;        
        $currentdate = time() + 7200;        
        $currentdatefordb = date('Y-m-d, H:m', $currentdate);

        $newfilename = $currentdate . "-" . $_SESSION["username"] . "." . $extension;
        $upload = $directory . $newfilename; 

        if (!file_exists($upload)) {            
            if (move_uploaded_file($file_temp, $upload)) {
                //echo "<p><b>Success!</b></p>";
                array_push($errors, 'Uspešan upload.');
            }
            else {
                array_push($errors, 'Došlo je do greške. Pokušajte ponovo.');
                //echo "<p><b>There was an error!</b></p>";
            }
        }
    }
    $subject = mysqli_real_escape_string($db, $_POST['subject']);
    $description = mysqli_real_escape_string($db, $_POST['description']);
    $price = mysqli_real_escape_string($db, $_POST['price']);
    $phone = mysqli_real_escape_string($db, $_POST['phone']);
    $category = $_POST['category'];
    $currency = $_POST['currency'];

    $query = "INSERT INTO advertise (subject, description, price, currency_id, phone, picture, date, category_id) VALUES ('$subject', '$description', '$price', (SELECT id FROM currency WHERE currency='$currency'), '$phone','$upload', '$currentdate', (SELECT id FROM category WHERE category='$category'));";
    /*$query = "INSERT INTO advertise (subject, description, price, picture, date, category_id) VALUES ('$subject', '$description', '$price', '$upload', '$currentdate', '1');";*/
    if (mysqli_query($db, $query)) {
        array_push($errors, "Oglas je unet uspešno!");        
    }
    else {
        //echo $query . "<br>" . mysqli_error($db);
        array_push($errors, "Error");        
    }
    //var_dump($errors);
    header('location: index.php'); //redirect to newad page
}


// logout
if (isset($_GET['logout'])) {
    session_destroy();
    unset($_SESSION['username']);
    header('location: index.php');
}

?>