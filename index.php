<?php include('server.php'); ?>

<!DOCTYPE html>
<html lang="sh">
    
<head>
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8">
    <link rel="stylesheet" type="text/css" href="css/style.css">
    <link rel="icon" href="images/favicon.jpg">
    <title>Project no. 10</title>
</head>

<body>
    <div class="container">
        <div class="header">
            <h1>Twilight<span class="off">Blue</span></h1>
            <h2>MALI OGLASI</h2>
        </div>

        <div class="menu">
            <ul>
                <li class="menuitem"><a href="#">Početna</a></li>
                <li class="menuitem"><a href="#">O nama</a></li>
                <li class="menuitem"><a href="#">Oglasi</a></li>
                <li class="menuitem"><a href="#">Usluge</a></li>
                <li class="menuitem"><a href="#">Zaposleni</a></li>
              <li class="menuitem"><a href="#">Kontakt</a></li>
            </ul>
        </div>

        <div class="leftmenu">
            <div class="leftmenu_top"></div>
            <div class="leftmenu_main">                
                <h3>Links</h3>
                <ul>
                    <li><a href="index.php">Početna</a></li>                    
                    <?php
                    if (empty($_SESSION['username'])) {
                        echo '<li><a href="login.php">Uloguj se</a></li>';
                        echo '<li><a href="register.php">Registruj nalog</a></li>';
                    }                        
                    else {
                        echo '<li><a href="newad.php">Dodaj novi oglas</a></li>';
                        echo '<p class="welcome">Dobrodošli, <strong>';
                        echo $_SESSION['username'];
                        echo '</strong></p>';
                        echo '<li><a href="index.php?logout=\'1\'" style="color:maroon">Izloguj se</a></li>';
                    }
                    ?>                 
                    
                </ul>
            </div>            
            <div class="leftmenu_bottom"></div>
        </div>
        <div class="content">
            <div class="content_top"></div>
            <div class="content_main">
                
                <?php            
                echo '<h2>Oglasi u bazi</h2><br><br>';
                $sql = "SELECT advertise.id, advertise.subject, advertise.description, advertise.price, currency.currency, advertise.phone, advertise.picture, advertise.date, category.category FROM advertise
                INNER JOIN currency ON advertise.currency_id = currency.id
                INNER JOIN category ON advertise.category_id = category.id
                ORDER by date DESC;";
                $result = mysqli_query($db, $sql) or die(mysqli_error($db));            

                if (mysqli_num_rows($result) > 0) {
                    // $counter instead of id because id sequence won't always be correct
                    $counter = 1;
                    while ($row = mysqli_fetch_assoc($result)) {                        
                        $oglas_id = $row["id"];                        
                        
                        echo '<div class="no-add">';
                        echo 'Oglas broj: ' . $counter;
                        echo '</div>';
                        
                        echo '<div class="category-add">';
                        echo 'Kategorija: ' . $row["category"];
                        echo '</div>';
                        
                        echo '<div class="price-add">';         
                        // format with groups of thousands
                        echo 'Cena: ' . number_format($row["price"], 2, '.', ',') . ' ' . $row["currency"];
                        echo '</div>';
                        
                        echo '<div class="subject-add">';
                        echo $row["subject"];
                        echo '</div>';
                        
                        echo '<div class="description-add">';
                        echo $row["description"];
                        echo '</div>';                                                
                        
                        echo '<div class="image-add">';
                        echo 'Slika:<br><br><img src="'.$row["picture"].'">';
                        echo '</div>';                          
                        
                        echo '<div class="date-add">';
                        echo 'Datum:' . date('d-m-Y, H:i:s', $row["date"]);
                        echo '</div>';
                        $counter++;
                    }                   
                }
                    echo '<div class="row">';
                    echo "Trenutan broj reklama u bazi: " . --$counter . ".<br>";
                    echo '</div>';
                ?>
            </div>
            <div class="content_bottom"></div>
            <div class="footer"><h3><a href="#">MySQL and PHP</a></h3></div>
        </div>
    </div>    
</body>
    
</html>
